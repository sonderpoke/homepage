const categories = ['Social', 'News', 'Media', 'Games', 'Programming'];

const links = {
  Social: [
    'https://www.instagram.com',
    'https://www.facebook.com',
    'https://www.messenger.com',
    'https://www.linkedin.com',
  ],
  News: [
    'https://www.reddit.com',
    'https://www.hackernoon.com',
    'https://www.15min.lt/naujienos/aktualu/pasaulis',
    'https://www.delfi.lt/news/daily/world/',
  ],
  Media: [
    'https://www.youtube.com',
    'https://www.twitch.tv/directory/game/Science%20%26%20Technology',
    'https://www.netflix.com',
    'https://open.spotify.com',
  ],
  Games: [
    'https://www.chess.com/play/online',
    'https://www.lichess.org/',
  ],
  Programming: [
    'https://www.gitlab.com',
    'https://www.github.com',
    'https://www.dzone.com',
  ],
};

const drawSets = (catName) => {
  const bookmarkSet = document.createElement('section');
  bookmarkSet.className = 'bookmark-set';

  const setHeader = document.createElement('header');
  setHeader.className = 'set-header';
  setHeader.innerHTML = catName;
  bookmarkSet.appendChild(setHeader);

  const bookmarkInnerContainer = document.createElement('nav');
  bookmarkInnerContainer.className = 'bookmark-inner-container';
  bookmarkInnerContainer.id = catName;
  bookmarkSet.appendChild(bookmarkInnerContainer);

  return bookmarkSet;
};

const drawContainers = () => {
  const bookmarksContainer = document.getElementById('bookmarksContainer');

  categories.forEach(cat => {
    this.bookmarksContainer.appendChild(drawSets(cat));
  });

  document.getElementById('mainContainer').appendChild(bookmarksContainer);
};

class Bookmark {
  constructor(link, shortName, rating) {
    this.link = link;
    this.shortName = shortName;

    if (rating && !Number.isNaN(rating)) {
      this.rating = rating;
    } else {
      this.rating = 0;
    }
  }
}

const extractRating = (shortName) => {
  return localStorage.getItem(shortName);
};

const generateBookmarks = (links) => {

  const bookmarks = [];

  for (let i = 0; i < links.length; i++) {
    const shortName = links[i].split('.')[1];
    const rating = extractRating(shortName);
    const bookmark = new Bookmark(links[i], shortName, rating);

    bookmarks.push(bookmark);
  }

  return bookmarks;
};

const sortBookmarksByRating = (bookmarks) => {
  if (!bookmarks) {
    return;
  }

  bookmarks.sort((b1, b2) => {
    if (parseInt(b1.rating) > parseInt(b2.rating)) {
      return -1;
    }
    if (parseInt(b1.rating) < parseInt(b2.rating)) {
      return 1;
    }
    if (parseInt(b1.rating) === parseInt(b2.rating)) {
      return 0;
    }
  });

  return bookmarks;
}

const appendLinks = (bookmarks, element) => {
  for (let i = 0; i < bookmarks.length; i++) {
    const linkButton = document.createElement('span');
    linkButton.className = 'link-button';

    const img = document.createElement('img');

    img.src = '/assets/icons/' + bookmarks[i].shortName + '.png';
    img.className = 'link-icon';

    const a = document.createElement('a');
    a.href = bookmarks[i].link;
    a.target = '_blank';
    a.className = 'bookmark';
    a.innerHTML = bookmarks[i].shortName;

    linkButton.onclick = () => {
      bookmarks[i].rating = Number(bookmarks[i].rating) + 1;
      console.log(bookmarks[i].rating);
      localStorage.setItem(bookmarks[i].shortName, bookmarks[i].rating);
      clearSets();
      fillSets();
    }

    linkButton.appendChild(img);
    linkButton.appendChild(a);
    element.appendChild(linkButton);
  }
}

const removeElChildrens = (el) => {
  while (el.firstChild) {
    el.removeChild(el.firstChild);
  }
};

const clearSets = () => {
  categories.forEach((catName) => {
    const el = document.getElementById(catName);
    removeElChildrens(el);
  });
};

const fillSets = () => {
  categories.forEach((catName) => {
    const el = document.getElementById(catName);
    const catBookmarks = sortBookmarksByRating(generateBookmarks(links[catName]));
    appendLinks(catBookmarks, el);
  });
}

window.addEventListener('load', drawContainers);
window.addEventListener('load', fillSets);

