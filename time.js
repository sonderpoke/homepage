const getTime = () => {
  const date = new Date(new Date().toLocaleString("en-US", {timeZone: "Europe/Vilnius"}));

  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();

  return (hours < 10 ? '0' + hours : hours) + ':' +
    (minutes < 10 ? '0' + minutes : minutes) + ':' +
    (seconds < 10 ? '0' + seconds : seconds);
}

const initTime = () => {
  document.getElementById('time').innerHTML = getTime();
  setInterval( () => {
    document.getElementById('time').innerHTML = getTime();
  }, 1000);
}

window.addEventListener('load', initTime);

