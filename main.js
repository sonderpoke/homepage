const isMobile = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
if (isMobile) {
  document.documentElement.style.setProperty('--clock-font-size', '8vw');   
  document.documentElement.style.setProperty('--set-header-font-size', '6vw');   
  document.documentElement.style.setProperty('--bookmark-font-size', '4vw');   
  document.documentElement.style.setProperty('--link-icon-height', '4vw');   
}
